<?php
class TeacherModel extends Database {
    public function insertTeacher($name, $avatar, $description, $specialized, $degree) {
        $res = $this -> dbInsert('teachers', array(
            'name' => $name,
            'avatar' => $avatar,
            'description' => $description,
            'specialized' => $specialized,
            'degree' => $degree,
            'updated' => date('Y-m-d H:i:s'),
            'created' => date('Y-m-d H:i:s'),
        ));

        if ($res) {
            return $this -> getLastTeacher();
        }
    }

    private function getLastTeacher() {
        return $this->dbGetrow("SELECT t.id, t.name, t.avatar, t.description, t.specialized, t.degree,
                t.updated, t.created 
                FROM teachers as t 
                ORDER BY id DESC limit 1"
            );
    }

    public function deleteTeacher($id){
        return $this->dbRemove("teachers", "id = '$id' ");
    }

    public function updateTeacher($id, $name, $avatar, $description, $specialized, $degree) {
        $res = $this -> dbUpdate('teachers', array(
            'name' => $name,
            'avatar' => $avatar,
            'description' => $description,
            'specialized' => $specialized,
            'degree' => $degree,
            'updated' => date('Y-m-d H:i:s'),
        ), "id = '$id'");

        if ($res) {
            return $this -> getTeacherById($id);
        }
    }

    public function getTeacherById($id) {
        return $this->dbGetrow("SELECT t.id, t.name, t.avatar, t.description, t.specialized, t.degree,
                t.updated, t.created 
                FROM teachers as t 
                WHERE t.id = '$id'"
            );
    }

    public function searchTeacher($specialized, $key) {
        $base_query = "SELECT * FROM teachers WHERE";
        
        if ($specialized) {
            $base_query = $base_query . " specialized = '$specialized' AND";
        }
        
        if ($key) {
            $base_query = $base_query . " (name LIKE '%$key%' OR description LIKE '%$key%' OR degree LIKE '%$key%') AND";
        }
        
        $base_query = $base_query . " 1";

        return $this->dbGetlist($base_query);
    }
}