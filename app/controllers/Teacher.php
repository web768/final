<?php

class Teacher extends Controller{
    private $teacherModel;
    function __construct() {
        $this->teacherModel = $this->model("TeacherModel");
    }

    function Index() {
        $res = [
            'router' => '',
            'title' => 'Teacher',
            'data' => null
        ];
        $this->view("teacher/base", $res);
    }

    function Add() {
        $res = [
            'router' => 'Add',
            'title' => 'Create Teacher',
            'data' => null
        ];
        $this->view("teacher/base", $res);
    }

    function AddConfirm() {
        if (!isset($_SESSION['user'])) {
            $data = null;
        }else {
            $data = [
                'teacher_name' => $_SESSION['user']['teacher_name'],
                'specialized' => _SPECIALIZED_VALUES[$_SESSION['user']['specialized']],
                'degree' => _DEGREE_VALUES[$_SESSION['user']['degree']],
                'description' => $_SESSION['user']['description'],
                'image' => '.'._UPLOAD_URL. $_SESSION['user']['image']
            ];
        }
        $res = [
            'router' => 'AddConfirm',
            'title' => 'Confirm Teacher',
            'data' => $data
        ];
        $this->view("teacher/base", $res);
    }

    function AddComplete() {
        if (!isset($_SESSION['user'])) {
            $data = null;
        }else {
            $data = [
                'teacher_name' => $_SESSION['user']['teacher_name'],
                'specialized' => $_SESSION['user']['specialized'],
                'degree' => $_SESSION['user']['degree'],
                'description' => $_SESSION['user']['description'],
                'image' => $_SESSION['user']['image']
            ];
        }
        $res = [
            'router' => 'AddComplete',
            'title' => 'Complete Add Teacher',
            'data' => $data
        ];
        $this->view("teacher/base", $res);
    }

}