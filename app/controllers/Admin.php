<?php
class Admin extends Controller {
    private $adminModel;
    function __construct() {
        $this->adminModel = $this->model("AdminModel");
    }
    public function Index() {
        $this->view("admin/login", null);
    }

    public function Login() {
        $this->view("admin/login", null);
    }

    public function ResetPassword() {
        $this->view("admin/resetPassword", null);
    }

    public function Logout(){
        $cookie_name = "login_id";
        $res = setcookie($cookie_name, null, time() - 3600, '/');
        unset($_COOKIE[$cookie_name]);

        $cookie_name = "login_time";
        setcookie($cookie_name, null, time() - 3600, "/");
        unset($_COOKIE[$cookie_name]);

        header("location:". $this->url()."/Admin");
    }

    function url(){
        if(isset($_SERVER['HTTPS'])){
            $protocol = ($_SERVER['HTTPS'] && $_SERVER['HTTPS'] != "off") ? "https" : "http";
        }
        else{
            $protocol = 'http';
        }
        return $protocol . "://" . $_SERVER['HTTP_HOST']."/final";
    }
}