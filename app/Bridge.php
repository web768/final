<?php
// Process URL from browser
require_once "./app/common/App.php";

// How controllers call Views & Models
require_once "./app/common/Controller.php";

// Connect Database
require_once "./app/common/Database.php";

require_once "./app/common/Constants.php";

?>