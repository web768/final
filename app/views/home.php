<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang chủ</title>

    <link rel="stylesheet" href="./web/css/home.css">
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

</head>
<body>
    <div class ='home'>
        <h5>Tên login: <?php echo $_COOKIE['login_id']; ?></h5>
        <h5 >Thời gian login: <?php echo $_COOKIE['login_time']; ?></h5>
        <div style="margin-bottom: 25px;">
            <label for="" class="text-label"><a href="Admin/Logout">Đăng xuất</a></label>
        </div>
        

        <table class="table table-hover">
            <thead>
                <tr>
                    <th scope="col">Phòng học</th>
                    <th scope="col">Giáo viên</th>
                    <th scope="col">Môn học</th>
                    <th scope="col">Sinh viên</th>
                    <th scope="col">Điểm</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><a href ='' >Tìm kiếm</a></td>
                    <td><a href ='Teacher/' >Tìm kiếm</a></td>
                    <td><a href ='' >Tìm kiếm</a></td>
                    <td><a href ='' >Tìm kiếm</a></td>
                    <td><a href ='' >Tìm kiếm</a></td>
                </tr>
                <tr>
                    <td><a href ='' >Thêm mới</a></td>
                    <td><a href ='Teacher/Add' >Thêm mới</a></td>
                    <td><a href ='' >Thêm mới</a></td>
                    <td><a href ='' >Thêm mới</a></td>
                    <td><a href ='' >Thêm mới</a></td>
                </tr>
            </tbody>
        </table>
    </div>

</body>
</html>