<?php
    require_once './app/models/TeacherModel.php';
    $teacher = new TeacherModel();
    $conn = mysqli_connect("localhost", "root", "", "final");

    if (!empty($_POST)) {
        $spec = "";
        $keyword = "";
        if (isset($_POST['specialized'])) {
            $spec = $_POST['specialized'];
        }

        if (isset($_POST['keyword'])) {
            $keyword = $_POST['keyword'];
        }
        $res = $teacher->searchTeacher($spec, $keyword);
    }
    else {
        $res = $teacher->searchTeacher('', '');
    }
?>

<div class="search-layout">
    <div class="search-box">
        <form id="search-form" action="" method="post">
            <div class="input-box">
            <label for="specialized" class="text-label">Bộ môn</label>
                <select name="specialized" id="specialized" class="select-field">
                    <option value="" disabled selected value>Chọn phân khoa</option>
                    <?php
                    foreach (_SPECIALIZED_VALUES as $key => $value) {
                        if ($_GET['specialized'] == $key) {
                            echo "<option value='$key' selected>$value</option>";
                        } else {
                            echo "<option value='$key'>$value</option>";
                        }
                    }
                    ?>
                </select>
            </div>
            <div class="input-box">
                <label for="" class="text-label">Từ khóa</label>
                <?php
                    if (isset($_GET['keyword'])) {
                        $keyword = $_GET['keyword'];
                    } else {
                        $keyword = "";
                    }
                    echo "<input type='text' name='keyword' id='keyword' class='text-field' value='$keyword'>";
                ?>
            </div>
            <div class="d-flex justify-content-center">
                <div class="p-2">
                    <div class="btn">
                        <input class="btn-submit" type="submit" value="Tìm kiếm"></input>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="student-count">
        <div class="student-found">
            <p><b>Số giáo viên tìm thấy: <?php echo count($res) ?></b></p>
        </div>
        <a href="Add" class="btn-submit">Thêm</a>
    </div>
    <div class="table-responsive" style="margin:auto">
        <table class="table table-hover">
            <thead>
                <tr>
                    <th style="width: 10%" scope="col">No</th>
                    <th style="width: 25%" scope="col">Tên giáo viên</th>
                    <th style="width: 15%" scope="col">Khoa</th>
                    <th style="width: 40%" scope="col">Mô tả chi tiết</th>
                    <th style="text-align:center;width: 10%" scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $i = 1;
                foreach ($res as $key => $value) {
                    $teacher_name = $value['name'];
                    $specialized = $value['specialized'];
                    $description = $value['description'];
                    $id = $value['id'];
                    echo "<tr>";
                    echo "<th scope='row'>$i</th>";
                    echo "<td>$teacher_name</td>";
                    echo "<td>"._SPECIALIZED_VALUES[$specialized]."</td>";
                    echo "<td>$description</td>";
                    echo "<td class='action'>";
                    echo "<buton class='btn-action'>Xóa</buton>";
                    echo "<buton class='btn-action'>Sửa</buton>";
                    echo "</td>";
                    echo "</tr>";
                    $i++;
                }
                ?>
            </tbody>
        </table>
    </div>
</div>