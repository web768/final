<?php
if (empty($data['data'])) {
    header("location:/final");
}
?>

<div class="register">
    <div class="register-infor">
        <?php
        require_once './app/models/TeacherModel.php';
        $teacher = new TeacherModel();

        $teacher_name = $data['data']['teacher_name'];
        $specialized = $data['data']['specialized'];
        $degree = $data['data']['degree'];
        $description = $data['data']['description'];
        $image_url = $data['data']['image'];

        $res = $teacher->insertTeacher($teacher_name, 
                                        $image_url, 
                                        $description, 
                                        $specialized, 
                                        $degree
                                    );

         
        if($res){
            echo "<h3>Bạn đã đăng kí thành công giáo viên</h3>";
            echo "<a href = ''>Quay lại trang chủ</a>";

        } else{
            echo "<h3>Có lỗi xảy ra khi thêm giáo viên</h3>";
            echo "<a href = ''>Quay lại trang chủ</a>";
        }
        session_destroy();
        ?>
    </div>
</div>
