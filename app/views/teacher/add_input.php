<?php
$errors = array();

if (!empty($_POST)) {
    
    if (($_POST["teacher_name"]) == '') {
        $errors[] = "Hãy nhập tên giáo viên";
    } 
    elseif (strlen($_POST["teacher_name"]) >= 100){
        $errors[] = "Không nhâp quá 100 ký tự";
    } 
    else {
        $_SESSION['user']['teacher_name'] = $_POST['teacher_name'];
    }
    
    if (($_POST["specialized"]) == '') {
        $errors[] = "Hãy chọn bộ môn";
    } else {
        $_SESSION['user']['specialized'] = $_POST['specialized'];
    }

    if (($_POST["degree"]) == '') {
        $errors[] = "Hãy chọn học vị";
    } else {
        $_SESSION['user']['degree'] = $_POST['degree'];
    }

    if (($_POST["description"]) == '') {
        $errors[] = "Hãy nhập mô tả chi tiết";
    } 
    elseif (strlen($_POST["description"]) >= 1000){
        $errors[] = "Không nhâp quá 1000 ký tự";
    } 
    else {
        $_SESSION['user']['description'] = $_POST['description'];
    }
    
    $upload_dir = _UPLOAD_URL;

    if (!file_exists($upload_dir)) {
        mkdir($upload_dir, 0777, true);
    }

    $img = $_FILES['image']['name'];

    // get ext of file
    $imageFileType = strtolower(pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION));

    // get name of file
    $imgName = basename($_FILES["image"]["name"], "." . $imageFileType);
    $file = $_FILES['image']['tmp_name'];

    // make new image name
    $target_file = $upload_dir . $imgName . "_" . date("YmdHis") . "." . $imageFileType;

    if ($_FILES['image']['size'] == 0) {
        $errors[] = "Hãy chọn avatar";
    } elseif ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
        $errors[] = "Chỉ được upload file JPG, JPEG, PNG";
    }else{
        if (isset($_POST["submit"]) && isset($file)){
            //  check real image
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            
            if($check == false) {
                $errors[] = "File đã chọn không phải ảnh";
            }else {
                move_uploaded_file($file, $target_file);
            }
        }
        $_SESSION['user']['image'] = $imgName . "_" . date("YmdHis") . "." . $imageFileType;
    }

    if (!$errors && count($_SESSION['user']) == 5) {
        header("location:AddConfirm");
    }
}
?>
<div class="register">
    <?php
    foreach ($errors as $error) {
        echo "<div class='alert alert-danger' role='alert'>$error</div>";
    }
    ?>
    <div class="register-infor">
        <form action="" enctype="multipart/form-data" method="post">
            <div class="input-box">
                <label for="" class="text-label">Họ và tên<span class="text-danger">*</span></label>
                <input type="text" class="text-field" name="teacher_name">
            </div>
            <div class="input-box">
                <label for="specialized" class="text-label">Chuyên ngành<span class="text-danger">*</span></label>
                <select name="specialized" id="specialized" class="select-field">
                    <option value=''></option>
                    <?php
                    foreach (_SPECIALIZED_VALUES as $key => $value) {
                        echo "<option value='$key'>$value</option>";
                    }
                    ?>
                </select>
            </div>
            <div class="input-box">
                <label for="degree" class="text-label">Học vị<span class="text-danger">*</span></label>
                <select name="degree" id="degree" class="select-field">
                    <option value=''></option>
                    <?php
                    foreach (_DEGREE_VALUES as $key => $value) {
                        echo "<option value='$key'>$value</option>";
                    }
                    ?>
                </select>
            </div>
            
            <div class="input-box" style="display: flex;">
                <label for="" class="text-label">Hình ảnh<span class="text-danger">*</span></label>
                <input type="file" id="img" class="image-field" name="image">
            </div>

            <div class="input-box">
                <label for="" class="text-label">Mô tả thêm<span class="text-danger">*</span></label>
                <textarea class="desc-area" id="description" name="description"></textarea>
            </div>
            <div class="btn">
                <button class="btn-submit" type="submit" name="submit">Xác nhận</button>
            </div>
        </form>
    </div>
</div>
