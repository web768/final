<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">

    <!-- base style  -->
    <!-- <link rel="stylesheet" href="../web/css/style.css"> -->
    <!-- bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <title><?php echo $data['title']?></title>
</head>
<style>
    <?php include_once(dirname(__FILE__).'/../../../web/css/style.css'); ?>
</style>
<body>
    <!-- CheckLogin -->
    <?php
        if(!(isset($_COOKIE["login_id"]))){
            header("location: /Admin/Login");   
        }
    ?>

    <?php

    if (isset($data)) {

        if (strcmp($data['router'], 'Add') == 0) {
            require_once "add_input.php";
        }
        elseif (strcmp($data['router'], 'AddConfirm') == 0)
        {
            require_once "add_confirm.php";
        }
        elseif (strcmp($data['router'], 'AddComplete') == 0)
        {
            require_once "add_complete.php";
        }
        else
        {
            require_once "search.php";
        }
    }

    if (strcmp($data['router'], '') == 0) {
        require_once "search.php";
    }
    
    ?>
</body>
</html>