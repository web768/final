<?php
if (!empty($_POST)) {
    header("location:AddComplete");
}
if (empty($data['data'])) {
    header("location:Add");
}
?>

<div class="confirm">
    <form action="#" enctype="multipart/form-data" method="post">
        <div class="input-box">
            <label for="" class="text-label">Họ và tên</label>
            <p><?php echo $data['data']['teacher_name']; ?></p>
        </div>
        <div class="input-box">
            <label for="" class="text-label">Chuyên ngành</label>
            <p><?php echo $data['data']['specialized']; ?></p>
        </div>
        <div class="input-box">
            <label for="department" class="text-label">Học vị</label>
            <p><?php echo $data['data']['degree']; ?></p>
        </div>
        <div class="input-box image-box">
            <label for="" class="text-label"> Hình ảnh </label>
            <?php
                echo '<img src="' .$data['data']['image'] . '">';
            ?>
        </div>
        <div class="input-box">
            <label for="department" class="text-label">Mô tả thêm</label>
            <p><?php echo $data['data']['description']; ?></p>
        </div>
        <div class="btn">
            <input class="btn-submit" type="button" value="Sửa lại" style = "margin-right: 12em;" onclick="history.back()">
            <button class="btn-submit" type="submit" name="submit">Xác nhận</button>
        </div>
    </form>
</div>
