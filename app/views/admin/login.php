<!DOCTYPE html>
<html lang="en">
<head>
    <title>Đăng nhập</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <!-- <link rel="stylesheet" href="../web/css/style.css"> -->
</head>

<style>
    <?php include_once(dirname(__FILE__).'/../../../web/css/style.css'); ?>
</style>

<?php
    $adminModel = new AdminModel();
    if (isset($_COOKIE["login_id"])) {
        header("location:/final");
    }

    $errors = array();
    if (!empty($_POST)) {
        if (($_POST["login_id"]) == '') {
            $errors[] = "Hãy nhập giá trị login id";
        } 
        elseif (strlen($_POST["login_id"]) < 4){
            $errors[] = "Login id tối thiểu 4 kí tự";
        }

        if (($_POST["password"]) == '') {
            $errors[] = "Hãy nhập password";
        } 
        elseif (strlen($_POST["password"]) < 6){
            $errors[] = "Password tối thiểu 6 kí tự";
        }

        if (empty($errors)) {
            $login_id = $_POST["login_id"];
            $password = $_POST["password"];
            $admin = $adminModel->AdminLogin($login_id, $password);
            if ($admin != null) {
                $cookie_name = "login_id";
                $cookie_value = $admin['login_id'];
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
                $cookie_name = "login_time";
                $cookie_value = date('Y-m-d H:i:s');
                setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/"); // 86400 = 1 day
                header("location:/final");
            } else {
                $errors[] = "Sai tên đăng nhập hoặc mật khẩu";
            }
        }
    }
?>
<body>
    <div class="register">
        <?php
            foreach ($errors as $error) {
                echo "<div class='alert alert-danger' role='alert'>$error</div>";
            }
        ?>
        <div class=register-infor>
            <form method='POST' action="">
                <div class="input-box">
                    <label for="" class="text-label">Người dùng<span class="text-danger">*</span></label>
                    <input type="text" class="text-field" name="login_id">
                </div>
                <div class="input-box">
                    <label for="" class="text-label">Password<span class="text-danger">*</span></label>
                    <input type="password" class="text-field" name="password">
                </div>
                <div class="input-box">
                    <a href=""><em>Quên password</em></a>       
                </div>   
                <div class="btn">
                    <button class="btn-submit" type="submit" name="submit">Đăng Nhập</button>
                </div>
            </form>
        </div>
    </div>
</body>
</html>
